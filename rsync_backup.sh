#!/bin/bash


USER=maja
USER=torsten

# backup directory


PORT=22

# host
IP=10.1.1.30

if [ -z $1 ] ; then
  exit
fi


help() {
   echo "usage:"
   echo "-v --verbose"
   echo "-h|--help"
   echo "-s|ssh  encrypt data transfer via ssh"
}

exem () {
  echo 'rsync_backup.sh -u maja -i 10.1.1.30'
}

# A leading ':' in the getopts string suppresses some default error
# messages A ':' after an option letter indicates an option $OPTARG is expected.
# OPTIND is the index of current OPTARG
# script is index 1


while getopts "hi:u:" opt ; do
  case $opt in
    h)  h=0 ;;
    u) u=0
        user=$OPTARG ;;
    i) i=0
       ip=$OPTARG
    *)  exem ; exit ;;
  esac
done



if [ $u ]; then
  echo "user is $user"
fi

#if [ $u && $i ] ; then
#  echo ' good to go'
#fi

if [ $h ] ; then
  help
  exit
fi

# local backup directory
LOCAL=$HOME/backup/

REMOTEDIR='/home/maja/filer'

if ! [ -d $LOCAL ] ; then
  echo "$LOCAL does not exist"
  exit
fi



if [ $v ]; then
  echo 'versbose'
  exit
fi

# --dry-run - This option simulates the backup. Useful to test its execution.
# rsync --archive -v --progress --delete --human-readable -e "ssh -p $PORT" \



# --verbose, -v    increase verbosity
# --delete         delete extraneous files from dest dirs
# --human-readable, -h
# backup remote files to local directory
ssh () {
  rsync -e ssh -hvP --delete -a $USER@$IP:$REMOTEDIR  $LOCAL
}


sync () {
  rsync -av --delete  ${USER}@${IP}:~/filer  fabriken/
}

dry () {
  rsync -av --delete  --dry-run ${USER}@${IP}:~/filer/  fabriken/
}


if [ $d ] ; then
  dry
fi




rsync() {
  echo "   _ __ ___ _   _ _ __   ___ "
  echo "  | '__/ __| | | | '_ \ / __|"
  echo "  | |  \__ \ |_| | | | | (__ "
  echo "  |_|  |___/\__, |_| |_|\___|"
  echo "            |___/            "
}
